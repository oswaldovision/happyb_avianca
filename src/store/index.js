// import firebase from 'firebase'
//
// const firebaseConfig = {
//   apiKey: 'AIzaSyA0xm_-BKJGFMfFW0Zr75ubhumnnN4CxKg',
//   authDomain: 'happybavianca.firebaseapp.com',
//   databaseURL: 'https://happybavianca.firebaseio.com',
//   projectId: 'happybavianca',
//   storageBucket: 'happybavianca.appspot.com',
//   messagingSenderId: '337577184197',
//   appId: '1:337577184197:web:97b2fb444fa13a5da719ac',
//   measurementId: 'G-KDR6750RVL'
// }
//
// Initialize Firebase
// firebase.initializeApp(firebaseConfig)
//
// const database = firebase.database()
// const dbRefMessages = database.ref('messages')
// const dbRefLikes = database.ref('likes')

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 25,
  },
  mutations: {
    increment (state) {
      state.count++
    },
    decrement (state) {
      if (state.count > 0) {
        state.count--
      }
    },
  }
})

export default store
