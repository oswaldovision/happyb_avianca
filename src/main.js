import Vue from 'vue'
import App from './App.vue'
import Bootstrap from 'bootstrap-vue'
import store from './store'

Vue.config.productionTip = false
Vue.use(Bootstrap)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
